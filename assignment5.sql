-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 05. Nov, 2017 11:51 AM
-- Server-versjon: 10.1.25-MariaDB
-- PHP Version: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `assignment5`
--

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `club`
--

CREATE TABLE `club` (
  `id` varchar(16) NOT NULL,
  `clubName` varchar(16) NOT NULL,
  `cityName` varchar(16) NOT NULL,
  `countyName` varchar(16) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `has`
--

CREATE TABLE `has` (
  `club.id` varchar(16) NOT NULL,
  `season.fallYear` year(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `season`
--

CREATE TABLE `season` (
  `skier.userName` varchar(16) NOT NULL,
  `club.id` varchar(16) NOT NULL,
  `fallYear` year(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `skier`
--

CREATE TABLE `skier` (
  `userName` varchar(16) NOT NULL,
  `firstName` varchar(16) NOT NULL,
  `lastName` varchar(16) NOT NULL,
  `YearOfBirth` year(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `skis`
--

CREATE TABLE `skis` (
  `skier.userName` varchar(16) NOT NULL,
  `season.fallYear` year(4) NOT NULL,
  `totalDistance` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `club`
--
ALTER TABLE `club`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `has`
--
ALTER TABLE `has`
  ADD PRIMARY KEY (`club.id`,`season.fallYear`),
  ADD KEY `season.fallYear` (`season.fallYear`);

--
-- Indexes for table `season`
--
ALTER TABLE `season`
  ADD PRIMARY KEY (`skier.userName`,`club.id`,`fallYear`),
  ADD UNIQUE KEY `fallYear` (`fallYear`),
  ADD KEY `club.id` (`club.id`);

--
-- Indexes for table `skier`
--
ALTER TABLE `skier`
  ADD PRIMARY KEY (`userName`);

--
-- Indexes for table `skis`
--
ALTER TABLE `skis`
  ADD PRIMARY KEY (`skier.userName`,`season.fallYear`),
  ADD KEY `season.fallYear` (`season.fallYear`);

--
-- Begrensninger for dumpede tabeller
--

--
-- Begrensninger for tabell `has`
--
ALTER TABLE `has`
  ADD CONSTRAINT `has_ibfk_1` FOREIGN KEY (`club.id`) REFERENCES `club` (`id`),
  ADD CONSTRAINT `has_ibfk_2` FOREIGN KEY (`season.fallYear`) REFERENCES `season` (`fallYear`);

--
-- Begrensninger for tabell `season`
--
ALTER TABLE `season`
  ADD CONSTRAINT `season_ibfk_1` FOREIGN KEY (`club.id`) REFERENCES `club` (`id`),
  ADD CONSTRAINT `season_ibfk_2` FOREIGN KEY (`skier.userName`) REFERENCES `skier` (`userName`);

--
-- Begrensninger for tabell `skis`
--
ALTER TABLE `skis`
  ADD CONSTRAINT `skis_ibfk_1` FOREIGN KEY (`season.fallYear`) REFERENCES `season` (`fallYear`),
  ADD CONSTRAINT `skis_ibfk_2` FOREIGN KEY (`skier.userName`) REFERENCES `skier` (`userName`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
