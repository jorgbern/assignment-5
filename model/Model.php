<?php
include_once("Skier.php");
include_once("Club.php");
include_once("Season.php");

class Model {        
    protected $db = null;  
	protected $doc;
    public function __construct($db = null)  
    {  
    $this->db = new PDO('mysql:host=localhost;dbname=assignment5;charset=utf8mb4', 'root', '');
	$this->doc = new DOMDocument();
	$this->doc->load('C:\xampp\htdocs\IMT2571\assignment5\SkierLogs.xml');
    }

	
	public function getSkiers() {
		$xpath = new DOMXpath($this->doc);
		$skierList = array();
		$request = '//SkierLogs/Skiers/Skier';
		$skiers = $xpath->query($request);
	
		foreach ($skiers as $skier) {
			$userName = $skier->getAttribute('userName');
		
			$firstName = $skier->getElementsByTagName('FirstName');
			$valueFirstName = $firstName->item(0)->nodeValue;
		
			$lastName = $skier->getElementsByTagName('LastName');
			$valueLastName = $lastName->item(0)->nodeValue;
		
			$yearOfBirth = $skier->getElementsByTagName('YearOfBirth');
			$valueYearOfBirth = $yearOfBirth->item(0)->nodeValue;
			
			$skierList[] = new Skier($userName, $valueFirstName, $valueLastName, $valueYearOfBirth);
		}
		return $skierList;
	}
	
	public function getClubs() {
		$xpath = new DOMXpath($this->doc);
		$clubList = array();
		$request = '//SkierLogs/Clubs/Club';
		$clubs = $xpath->query($request);
	
		foreach ($clubs as $club) {
			$id = $club->getAttribute('id');
	
			$name = $club->getElementsByTagName('Name');
			$valueName = $name->item(0)->nodeValue;
	
			$cityName = $club->getElementsByTagName('City');
			$valueCity = $cityName->item(0)->nodeValue;
	
			$countyName = $club->getElementsByTagName('County');
			$valueCounty = $countyName->item(0)->nodeValue;
			
			$clubList[] = new Club($id, $valueName, $valueCity, $valueCounty);
		}
		return $clubList;
	}


	public function getSeasons() {
		$xpath = new DOMXpath($this->doc);
		$seasonList = array();
		$request = '//SkierLogs/Season/Skiers/Skier';
		$skiers = $xpath->query($request);
	
		foreach($skiers as $skier) {
			$userName = $skier->getAttribute('userName');
			
			$totalDistance = 0;
			$entries = $skier->getElementsByTagName('Entry');
			foreach($entries as $entry) {
				$distanceElement = $entry->getElementsByTagName('Distance');
				$distance = $distanceElement->item(0)->nodeValue;
				$totalDistance += $distance;
			}
			
			$parentSkier = $skier->parentNode;
			$clubId = $parentSkier->getAttribute('clubId');
			
			$parentClub = $parentSkier->parentNode;
			$fallYear = $parentClub->getAttribute('fallYear');
			
			$seasonList[] = new Season($userName, $clubId, $fallYear, $totalDistance);
		}
		return $seasonList;
	} 

	
	public function addSkiers($skierList) {
		foreach($skierList as $skier) {
			try {
				$stmt = $this->db->prepare("INSERT INTO skier (userName, firstName, lastName, yearOfBirth) VALUES (:userName, :firstName, :lastName, :yearOfBirth)");
				$stmt->bindParam(':userName', $skier->userName);
				$stmt->bindParam(':firstName', $skier->firstName);
				$stmt->bindParam(':lastName', $skier->lastName);
				$stmt->bindParam(':yearOfBirth', $skier->yearOfBirth);
				$stmt->execute();
			} catch(PDOException $ex) {
				echo "Error.";
			}
		}
		
	}
	
	public function addClubs($clubList) {
		foreach($clubList as $club) {
			try {
				$stmt = $this->db->prepare("INSERT INTO club (id, clubName, cityName, countyName) VALUES (:id, :clubName, :cityName, :countyName)");
				$stmt->bindParam(':id', $club->id);
				$stmt->bindParam(':clubName', $club->name);
				$stmt->bindParam(':cityName', $club->city);
				$stmt->bindParam(':countyName', $club->county);
				$stmt->execute();
			} catch(PDOException $ex) {
				echo "Error.";
			}
		}
	}
	
	public function addSeasons($seasonList) {
		foreach($seasonList as $season) {
			try {
				$stmt = $this->db->prepare("INSERT INTO `season` (`skier.userName`, `club.id`, fallYear) VALUES (:userName, :clubId, :fallYear)");
				$stmt->bindParam(':userName', $season->userName);
				$stmt->bindParam(':clubId', $season->clubId);
				$stmt->bindParam(':fallYear', $season->fallYear);
				$stmt->execute();

				$stmt1 = $this->db->prepare("INSERT INTO `has` (`club.id`, `season.fallYear`) VALUES (:clubId, :fallYear)");
				$stmt1->bindParam(':clubId', $season->clubId);
				$stmt1->bindParam(':fallYear', $season->fallYear);
				$stmt1->execute();
				
				$stmt2 = $this->db->prepare("INSERT INTO `skis` (`skier.userName`, `season.fallYear`, totalDistance) VALUES (:userName, :fallYear, :totalDistance)");
				$stmt2->bindParam(':userName', $season->userName);
				$stmt2->bindParam(':fallYear', $season->fallYear);
				$stmt2->bindParam(':totalDistance', $season->totalDistance);
				$stmt2->execute();
			} catch(PDOException $ex) {
				echo "Error.";
			}
		}
	}
}
?>