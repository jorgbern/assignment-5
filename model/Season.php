<?php

class Season {
	public $userName;
	public $clubId;
	public $fallYear;
	public $totalDistance;

	public function __construct($userName, $clubId, $fallYear = -1, $totalDistance = -1) {
	$this->userName = $userName;
	$this->clubId = $clubId;
	$this->fallYear = $fallYear;
	$this->totalDistance = $totalDistance;
	}
}
?>