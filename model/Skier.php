<?php

class Skier {
	public $userName;
	public $firstName;
	public $lastName;
	public $yearOfBirth;

	public function __construct($userName, $firstName, $lastName, $yearOfBirth = -1) {
	$this->userName = $userName;
	$this->firstName = $firstName;
	$this->lastName = $lastName;
	$this->yearOfBirth = $yearOfBirth;
	}
}
?>