<?php

include_once("model/Club.php");
include_once("model/Skier.php");
include_once("model/Season.php");
include_once("model/Model.php");

class Controller {
	public $model;
	
	public function __construct() {
		session_start();
		$this->model = new Model();
	}
	
	public function invoke() {
		$tempSkiers = array();
		$tempSkiers = $this->model->getSkiers();
		$this->model->addSkiers($tempSkiers);
		$tempClubs = array();
		$tempClubs = $this->model->getClubs();
		$this->model->addClubs($tempClubs);
		$tempSeasons = array();
		$tempSeasons = $this->model->getSeasons();
		$this->model->addSeasons($tempSeasons);
	}
}
?>